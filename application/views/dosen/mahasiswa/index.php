<div class="content-wrapper"> 
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>admin/dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active"><?= $title ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Default box -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <!-- <h3 class="card-title">Data <?= $title ?></h3> -->
              <h3 class="card-title"><button type="button" class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus"></i> Tambah Data</button></h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 10px">No</th>
                    <th>Nama Mahasiswa</th>
                    <th>Npm</th>
                    <th>Prodi</th>
                    <th>Mata Kuliah yang diambil</th>
                    <th style="width: 200px">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($rs_mahasiswa as $key => $value) { ?>
                    <tr>
                      <td align="center"><?= $no++ ?></td>
                      <td><?= $value['nama_mahasiswa'] ?></td>
                      <td><?= $value['npm'] ?></td>
                      <td><?= $value['nama_prodi'] ?></td>
                      <td>
                        <?php foreach ($value['list_krs'] as $key2 => $value2) { ?>
                          <?= ($key2+1).'. '.$value2['nama_matkul'] ?> <br>
                        <?php }  ?>
                      </td>
                      <td align="center">
                        <a class="btn btn-info btn-sm approve_krs" href="#" onclick="approve(<?= $value['id_krs'] ?>)" ><i class="fas fa-list"></i> Aksi</a>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
              <ul class="pagination pagination-sm m-0 float-right">
                <?=  $this->pagination->create_links(); ?>
                <!-- <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">&raquo;</a></li> -->
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<div class="modal fade" id="modal-approve">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?= $title ?></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url() ?>admin/dosen/update" method="POST">
        <input type="hidden" name="id_dosen" id="id_dosen" value="">
        <input type="hidden" name="id_user" id="id_user" value="">
        <div class="modal-body">

          <div class="form-group">
            <label for="nama_dosen">Nama Dosen</label>
            <input type="text" class="form-control" name="nama_dosen" id="nama_dosen_edit" placeholder="Nama Dosen" required>
          </div>
          
          <div class="form-group">
            <label for="nip">Nip</label>
            <input type="text" class="form-control" name="nip" id="nip_edit" placeholder="Nip" required>
          </div>

          <div class="form-group">
            <label for="alamat">Alamat</label>
            <input type="text" class="form-control" name="alamat" id="alamat_edit" placeholder="Alamat" required>
          </div>

          <div class="form-group">
            <label for="alamat">Password</label>
            <input type="password" class="form-control" name="password" id="password_edit" placeholder="Password" required>
          </div>

        </div>

        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Update</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script>
  function approve(id_krs) {
    alert(id_krs);
    $('#modal-approve').modal('show');
  }
</script>