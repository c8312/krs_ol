<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        cek_login('admin');
        // model
        // $this->load->model('Admin_model', 'admin');
    }


	public function index()
	{
		$data['title'] = 'Dashboard';
		$this->load->view('template/admin/header', $data);
		$this->load->view('admin/dashboard/index');
		$this->load->view('template/admin/footer');

	}

}