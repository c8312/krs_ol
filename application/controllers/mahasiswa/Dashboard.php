<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        cek_login('mahasiswa');
        // model
        // $this->load->model('Admin_model', 'admin');
    }


	public function index()
	{
		$data['title'] = 'Dashboard';
		$this->load->view('template/mahasiswa/header', $data);
		$this->load->view('mahasiswa/dashboard/index');
		$this->load->view('template/mahasiswa/footer');

	}

}