<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        cek_login('dosen');
        // model
        $this->load->model('dosen/M_dosen', 'dosen');
    }


	public function index()
	{
		$data_user = $this->session->userdata('login_session');
		/* start of pagination --------------------- */
        $this->load->library('pagination');
        // pagination
        $config['base_url'] = site_url('/dosen/mahasiswa/');
		$config['page_query_string'] = TRUE;
		$config['total_rows'] = $this->dosen->get_total_data($data_user['user']);
		$config['per_page'] = 5;

		$config['attributes'] = array('class' => 'page-link');
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';

		$this->pagination->initialize($config);
		$limit = $config['per_page'];
		$offset = (int) html_escape($this->input->get('per_page'));

		$data['no'] = $offset+1;

        // data dosen
        $params = array($data_user['user'], $offset, $limit);
		$data['rs_mahasiswa'] = $this->dosen->get_all_data_mahasiswa($params);

		$data['title'] = 'Approve Krs';
		$this->load->view('template/dosen/header', $data);
		$this->load->view('dosen/mahasiswa/index');
		$this->load->view('template/dosen/footer');

	}

}