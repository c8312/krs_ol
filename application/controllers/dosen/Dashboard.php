<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        cek_login('dosen');
        // model
        // $this->load->model('dosen_model', 'dosen');
    }


	public function index()
	{
		$data['title'] = 'Dashboard';
		$this->load->view('template/dosen/header', $data);
		$this->load->view('dosen/dashboard/index');
		$this->load->view('template/dosen/footer');

	}

}